<?php

namespace ASI_lytics\Http\Controllers;

use Illuminate\Http\Request;

class QuestionnaireController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('questionnaires.index', [
        
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('questionnaire.createQuestionnaire');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tutoriel = $request->user()->create([
            'nomQuestionnaire' => $request->input('descriptionTuto')
        ]);
        return redirect()->route('questionnaires.show', [$questionnaire]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \ASI_lytics\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function show(Questionnaires $questionnaire)
    {
        return view('questionnaire.show', [
            'questionnaire' => $questionnaire
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \ASI_lytics\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function edit(Questionnaire $questionnaire)
    {
        return view('questionnaire.update', [
            'questionnaire' => $questionnaire
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \ASI_lytics\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Questionnaire $questionnaire)
    {
        $tutoriel->update([
            'nomQuestionnaire' => $request->input('descriptionTuto')
        ]);
        return redirect()->route('questionnaires.show', [$questionnaire]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \ASI_lytics\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function destroy(Questionnaires $questionnaire)
    {
        $questionnaire->delete();
        return redirect()->route('questionnaires.index');
    }
}
