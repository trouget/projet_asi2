<?php

namespace ASI_lytics\Http\Controllers;

use Illuminate\Http\Request;

class TopicController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = Topic::all();
        return view('topics.index', compact('topics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('topic.createTopic');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'sujetTopic'=>'required',
            'descriptionTopic'=>'required'
        ]);
        $contact = new Topic([
            'sujetTopic' => $request->get('sujetTopic'),
            'descriptionTopic' => $request->get('descriptionTopic')
        ]);
        $contact->save();
        return redirect('/topics')->with('success', 'Topic saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idTopic)
    {
        $topic = Topic::find($idTopic);
        return view('topics.editTopics', compact('topics'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idTopic)
    {
        $request->validate([
            'sujetTopic'=>'required',
            'descriptionTopic'=>'required',
        
        ]);
        $topic = Topic::find($idTopic);
        $topic->sujetTopic =  $request->get('sujetTopic');
        $topic->descriptionTopic = $request->get('descriptionTopic');
        $topic->save();
        return redirect('/topics')->with('success', 'Topic updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idTopic)
    {
        $topic = Topic::find($idTopic);
        $topic->delete();
        return redirect('/topics')->with('success', 'Topic deleted!');
    }
}
