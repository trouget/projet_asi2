<?php

namespace ASI_lytics\Http\Controllers;

use ASI_lytics\Tutoriel;
use Illuminate\Http\Request;

class TutorielController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tutoriels.index', [
        
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tutoriels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tutoriel = $request->user()->create([
            'titreTuto' => $request->input('titreTuto'),
            'descriptionTuto' => $request->input('descriptionTuto'),
            'date' => $request->input('date'),
            'categorie' => $request->input('categorie'),
            'auteur' => $request->input('auteur')
        ]);
        return redirect()->route('tutoriels.show', [$tutoriel]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \ASI_lytics\Tutoriel  $tutoriel
     * @return \Illuminate\Http\Response
     */
    public function show(Tutoriel $tutoriel)
    {
        return view('tutoriel.show', [
            'tutoriel' => $tutoriel
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \ASI_lytics\Tutoriel  $tutoriel
     * @return \Illuminate\Http\Response
     */
    public function edit(Tutoriel $tutoriel)
    {
        return view('tutoriel.update', [
            'tutoriel' => $tutoriel
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \ASI_lytics\Tutoriel  $tutoriel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tutoriel $tutoriel)
    {
        $tutoriel->update([
            'titreTuto' => $request->input('titreTuto'),
            'descriptionTuto' => $request->input('descriptionTuto'),
            'date' => $request->input('date'),
            'categorie' => $request->input('categorie'),
            'auteur' => $request->input('auteur')
        ]);
        return redirect()->route('tutoriels.show', [$tutoriel]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \ASI_lytics\Tutoriel  $tutoriel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tutoriel $tutoriel)
    {
        $tutoriel->delete();
        return redirect()->route('tutoriels.index');
    }
}
