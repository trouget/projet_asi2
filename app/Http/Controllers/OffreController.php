<?php

namespace ASI_lytics\Http\Controllers;

use Illuminate\Http\Request;

class OffreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offres = Offre::all();
        return view('offres.index', compact('offres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('offres.createOffres');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'titreOffre'=>'required',
            'niveauEtude'=>'required',
            'pdf'=>'required',
            'descriptionOffre'=>'required'
        ]);
        $contact = new Offre([
            'titreOffre' => $request->get('titreOffre'),
            'niveauEtude' => $request->get('niveauEtude'),
            'pdf' => $request->get('pdf'),
            'descriptionOffre' => $request->get('descriptionOffre')
        ]);
        $contact->save();
        return redirect('/offres')->with('success', 'Offre saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idOffre)
    {
        $offre = Offre::find($idOffre);
        return view('offres.editOffres', compact('offres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idOffre)
    {
        $request->validate([
            'titreOffre'=>'required',
            'niveauEtude'=>'required',
            'pdf'=>'required',
            'descriptionOffre'=>'required'
        ]);
        $offre = Offre::find($idOffre);
        $offre->titreOffre =  $request->get('titreOffre');
        $offre->niveauEtude = $request->get('niveauEtude');
        $offre->pdf = $request->get('pdf');
        $offre->descriptionOffre = $request->get('descriptionOffre');
        $offre->save();
        return redirect('/offres')->with('success', 'Offre updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idOffre)
    {
        $offre = Offre::find($idOffre);
        $offre->delete();
        return redirect('/offres')->with('success', 'Offre deleted!');
    }
}
