<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTutoriel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutoriel', function (Blueprint $table) {
            $table->increments('idTuto');
            $table->string('titreTuto');
            $table->string('descriptionTuto');
            $table->string('date');
            $table->string('categorie');
            $table->string('auteur');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutoriel');
    }
}
