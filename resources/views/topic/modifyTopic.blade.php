@extends('template/templateAdmin')

@section('content')
<table class="table table-bordered">
    <thead class="thead-inverse">
        <tr>
            <th style="width: 2px">#</th>
            <th style="width: 10px">Sujet</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($LesTopics as $unTopic)
        <tr>
            <td class="col-md-1">
                {{ $unTopic["idTopic"] }}
            </td>

            <td class="col-md-1">
                {{ $unTopic["sujetTopic"] }}
            </td>
            <td class="col-md-2">
                {{ $unTopic["descriptionTopic"] }}
            </td>
        
            <td class="col-md-1">
            {{ Form::open(['route'=>['topic.destroy',$unTopic->idTopic], 'method' => 'delete']) }}
            {{Form::submit('Supprimez',["class"=>"btn btn-block btn-danger"])}}
            {{ Form::close() }}
            </td>
            <td class ="col-md-1">
            {{ Form::open(['route'=>['topic.edit',$unTopic->idTopic],"method"=>"get"]) }}
            {{Form::submit('Editer',["class"=>"btn btn-warning btn-block"])}}
            {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@stop