@extends('template/adminTemplate')
@section('content')
<!-- Main content -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            {!! Form::open(['route' => "topics.store", 'method' => 'post', 'files' => true]) !!}
            <div class="box-header">
                <h3 class="box-title">  </h3>

                <div class="form-group">
                    <label>Sujet:  </label>
                    <input class="form-control" placeholder="sujet" name="sujet">
                </div>
            </div>

            <div class="box-header">
                <h3 class="box-title">  </h3>

                <div class="form-group">
                    <label>Description:  </label>
                    <input class="form-control" placeholder="description" name="description">
                </div>
            </div>


            <button type="submit" class="btn btn-success btn-lg btn-block">Créer</button>

            {!! Form::close() !!}
        </div>
        <!-- /.box -->
    </div>
</div>

<!--script elfinder -->
<script type="text/javascript" src="/packages/barryvdh/elfinder/js/standalonepopup.min.js"></script>
@stop