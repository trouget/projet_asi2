@extends('template/adminTemplate')
@section('content')
<!-- Main content -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            {!! Form::open(['route' => ["offres.edit", $offre["idOffre"]], 'method' => 'put']) !!}
            <div class="box-header">
                <h3 class="box-title">  </h3>

                <div class="form-group">
                    <label>Titre:  </label>
                    <input class="form-control" name="nom" value={{$offre["titreOffre"]}}>
                </div>
            </div>

            <div class="box-header">
                <h3 class="box-title">  </h3>

                <div class="form-group" style="width:200px;">
                    <label for="role" class="control-label">Niveau d'étude</label>
                       <select class="form-control" id="niveauEtude" name="niveauEtude">
                       <option value="1">Bac+2</option>
                       <option value="2">Bac+3</option>
                       <option value="3">Bac+4</option>
                       <option value="4">Bac+5</option>
                       <option value="5">Autres</option>
                    </select>
                   </div>

                <div class="form-group">
                    <label> modifier le PDF:  </label>
                    <input class="form-control" name="prenom" value={{$offre["pdf"]}}>
                </div>
            </div>

            <div class="box-header">
                <h3 class="box-title">  </h3>

                <div class="form-group">
                    <label>Description:  </label>
                    <input class="form-control" name="description" value={{$offre["descriptionOffre"]}}>
                </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body pad">

                <div class="form-group">
                    {{ Form::textarea('editor', $offre["descriptionOffre"],['id'=>'editor','class'=>'form-control']) }}
                </div>

            </div>

            <button type="submit" class="btn btn-success btn-lg btn-block">Modifier</button>

            {!! Form::close() !!}
        </div>
        <!-- /.box -->
    </div>
</div>
@stop