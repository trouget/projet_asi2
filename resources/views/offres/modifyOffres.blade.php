@extends('template/adminTemplate')

@section('content')
<table class="table table-bordered">
    <thead class="thead-inverse">
        <tr>
            <th style="width: 2px">#</th>
            <th style="width: 10px">Titre</th>
            <th>Niveau d'Etude</th>
            <th>PDF</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($LesOffres as $uneOffre)
        <tr>
            <td class="col-md-1">
                {{ $uneOffre["idOffre"] }}
            </td>

            <td class="col-md-1">
                {{ $uneOffre["titreOffre"] }}
            </td>
            <td class="col-md-2">
                {{ $uneOffre["niveauEtude"] }}
            </td>
            <td class="col-md-4">
                {{ $uneOffre["pdf"] }}
            </td>
            <td class="col-md-4">
                {{ $uneOffre["descriptionOffre"] }}
            </td>

            <td class="col-md-1">
            {{ Form::open(['route'=>['offres.destroy',$uneOffre->idOffre], 'method' => 'delete']) }}
            {{Form::submit('Supprimez',["class"=>"btn btn-block btn-danger"])}}
            {{ Form::close() }}
            </td>
            <td class ="col-md-1">
            {{ Form::open(['route'=>['offres.edit',$uneOffre->idOffre],"method"=>"get"]) }}
            {{Form::submit('Editer',["class"=>"btn btn-warning btn-block"])}}
            {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@stop