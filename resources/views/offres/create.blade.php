@extends('base')
@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Crée une offre</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('offres.store') }}">
          @csrf
          <div class="form-group">    
              <label for="titreOffre">Titre:</label>
              <input type="text" class="form-control" name="titreOffre"/>
          </div>
          <div class="form-group">
              <label for="niveauEtude">Niveau d'étude:</label>
              <input type="text" class="form-control" name="niveauEtude"/>
          </div>
          <div class="form-group">
              <label for="pdf">PDF:</label>
              <input type="text" class="form-control" name="pdf"/>
          </div>
          <div class="form-group">
              <label for="descriptionOffre">Description:</label>
              <input type="text" class="form-control" name="descriptionOffre"/>
          </div>                         
          <button type="submit" class="btn btn-primary-outline">Ajouter</button>
      </form>
  </div>
</div>
</div>
@endsection