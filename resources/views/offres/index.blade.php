@extends('base')
@section('main')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Offres</h1>    
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Titre</td>
          <td>Etude</td>
          <td>PDF</td>
          <td>Description</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($offres as $offre)
        <tr>
            <td>{{$offre->idOffre}}</td>
            <td>{{$offre->titreOffre}}</td>
            <td>{{$offre->niveauEtude}}</td>
            <td>{{$offre->pdf}}</td>
            <td>{{$offre->descriptionOffre}}</td>
            <td>
                <a href="{{ route('offres.edit',$offre->idOffre)}}" class="btn btn-primary">Modifier</a>
            </td>
            <td>
                <form action="{{ route('offres.destroy', $offre->idOffre)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Supprimer</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
@endsection