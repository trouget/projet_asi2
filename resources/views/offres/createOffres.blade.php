@extends('template/adminTemplate')
@section('content')
<!-- Main content -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            {!! Form::open(['route' => "offres.store", 'method' => 'post', 'files' => true]) !!}
            <div class="box-header">
                <h3 class="box-title">  </h3>

                <div class="form-group">
                    <label>Titre:  </label>
                    <input class="form-control" placeholder="titre" name="titre">
                </div>
            </div>

            <div class="form-group" style="width:200px;">
                <label for="role" class="control-label">Niveau d'étude</label>
                   <select class="form-control" id="niveauEtude" name="niveauEtude">
                   <option value="1">Bac+2</option>
                   <option value="2">Bac+3</option>
                   <option value="3">Bac+4</option>
                   <option value="4">Bac+5</option>
                   <option value="5">Autres</option>
                </select>
               </div>

                <div class="form-group">
                    <label>Ajout d'un PDF:  </label>
                    <input class="form-control" placeholder="PDF" name="PDF">
                </div>
            </div>

            <div class="box-header">
                <h3 class="box-title">  </h3>
                <div class="form-group">
                    <label>Description:  </label>
                    <input class="form-control" placeholder="description" name="description">
                </div>
            </div>


            <!-- /.box-header -->
            <div class="box-body pad">

                <div class="form-group">
                    {{ Form::textarea('editor', '',['id'=>'editor','class'=>'form-control','placeholder'=>'Rentrer la description des animaux ici']) }}
                </div>

            </div>

            <div class="form-group">
                <a href="" class="popup_selector" data-inputid="feature_image"><label for="feature_image">Selectionner Image</label></a>                   
                <input type="text" id="feature_image" name="image" value="">
            </div>

            <button type="submit" class="btn btn-success btn-lg btn-block">Créer</button>

            {!! Form::close() !!}
        </div>
        <!-- /.box -->
    </div>
</div>

<!--script elfinder -->
<script type="text/javascript" src="/packages/barryvdh/elfinder/js/standalonepopup.min.js"></script>
@stop