@extends('template/templateAdmin')

@section('content')
<table class="table table-bordered">
    <thead class="thead-inverse">
        <tr>
            <th style="width: 2px">#</th>
            <th style="width: 10px">Titre</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($LesTutoriels as $unTutoriel)
        <tr>
            <td class="col-md-1">
                {{ $unTutoriel["id"] }}
            </td>

            <td class="col-md-1">
                {{ $unTutoriel["titre"] }}
            </td>
            <td class="col-md-2">
                {{ $unTutoriel["description"] }}
            </td>
            <td class="col-md-4">
                {{ $unTutoriel["date"] }}
            </td>
            <td class="col-md-4">
                {{ $unTutoriel["categorie"] }}
            </td>
            <td class="col-md-5">
                {{ $unTutoriel["auteur"] }}
            </td>
            <td class="col-md-1">
            {{ Form::open(['route'=>['tutoriels.destroy',$unTutoriel->id], 'method' => 'delete']) }}
            {{Form::submit('Supprimez',["class"=>"btn btn-block btn-danger"])}}
            {{ Form::close() }}
            </td>
            <td class ="col-md-1">
            {{ Form::open(['route'=>['tutoriels.edit',$unTutoriel->id],"method"=>"get"]) }}
            {{Form::submit('Editer',["class"=>"btn btn-warning btn-block"])}}
            {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@stop