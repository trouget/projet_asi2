@extends('template/templateAdmin')
@section('content')
<!-- Main content -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            {!! Form::open(['route' => ["tutoriels.update", $tutoriel["id"]], 'method' => 'put']) !!}
            <div class="box-header">
                <h3 class="box-title">  </h3>

                <div class="form-group">
                    <label>Titre  </label>
                    <input class="form-control" placeholder="tire" name="titre">
                </div>
            </div>

            <div class="box-header">
                <h3 class="box-title">  </h3>

                <div class="form-group">
                    <label>Decription  </label>
                    <input class="form-control" placeholder="description" name="description">
                </div>
            </div>

            <div class="box-header">
                <h3 class="box-title">  </h3>

                <div class="form-group">
                    <label>Date  </label>
                    <input class="form-control" placeholder="date" name="date">
                </div>

                <div class="form-group">
                    <label>Categories  </label>
                    <input class="form-control" placeholder="categorie" name="categorie">
                </div>

                <div class="form-group">
                    <label>Auteur  </label>
                    <input class="form-control" placeholder="auteur" name="auteur">
                </div>
            </div>

            <button type="submit" class="btn btn-success btn-lg btn-block">Modifier</button>

            {!! Form::close() !!}
        </div>
        <!-- /.box -->
    </div>
</div>
@stop