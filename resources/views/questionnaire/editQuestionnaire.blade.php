@extends('template/templateAdmin')
@section('content')
<!-- Main content -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            {!! Form::open(['route' => ["questionnaires.update", $questionnaire["idQuestionnaire"]], 'method' => 'put']) !!}
            <div class="box-header">
                <h3 class="box-title">  </h3>

                <div class="form-group">
                    <label>Questions:  </label>
                    <input class="form-control" name="nom" value={{$questionnaire["nomQuestionnaire"]}}>
                </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body pad">

                <div class="form-group">
                    {{ Form::textarea('editor', $questionnaire ["nomQuestionnaire"],['id'=>'editor','class'=>'form-control']) }}
                </div>

            </div>

            <button type="submit" class="btn btn-success btn-lg btn-block">Modifier</button>

            {!! Form::close() !!}
        </div>
        <!-- /.box -->
    </div>
</div>
@stop