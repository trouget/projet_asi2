@extends('template/templateAdmin')

@section('content')
<table class="table table-bordered">
    <thead class="thead-inverse">
        <tr>
            <th style="width: 2px">#</th>
            <th style="width: 10px">Questions</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($LesQuestionnaires as $unQuestionnaire)
        <tr>
            <td class="col-md-1">
                {{ $unQuestionnaire["idQuestionnaire"] }}
            </td>
            <td class="col-md-1">
                {{ $unQuestionnaire["nomQuestionnaire"] }}
            </td>
            <td class="col-md-1">
            {{ Form::open(['route'=>['questionnaire.destroy',$unQuestionnaire->idQuestionnaire], 'method' => 'delete']) }}
            {{Form::submit('Supprimez',["class"=>"btn btn-block btn-danger"])}}
            {{ Form::close() }}
            </td>
            <td class ="col-md-1">
            {{ Form::open(['route'=>['questionnaire.edit',$unQuestionnaire->idQuestionnaire],"method"=>"get"]) }}
            {{Form::submit('Editer',["class"=>"btn btn-warning btn-block"])}}
            {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@stop